package com.jpop.LibraryService.Controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.jpop.LibraryService.model.LibBooks;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@RestController
@RequestMapping("lib/books")
public class BookLibraryController {

	private static final String HTTP_LOCALHOST_4040_BOOKS = "http://BOOK-SERVICE/books/";
	
	@Autowired
	RestTemplate restTemplate;

	@GetMapping
	@HystrixCommand
	public List<LibBooks> getAllBooks() {
		LibBooks[] result = (LibBooks[]) restTemplate.getForObject(HTTP_LOCALHOST_4040_BOOKS, LibBooks[].class);
		return Arrays.asList(result);
	}

	@GetMapping("/{userId}")
	@HystrixCommand
	public LibBooks getBookById(@PathVariable Long userId) {
		return restTemplate.getForObject(HTTP_LOCALHOST_4040_BOOKS + userId, LibBooks.class);
	}

	@PostMapping()
	@HystrixCommand
	public LibBooks addBook( @RequestBody LibBooks book) {
		return restTemplate.postForObject(HTTP_LOCALHOST_4040_BOOKS , book, LibBooks.class);
	}

	@DeleteMapping("/{userId}")
	@HystrixCommand
	public void deleteBookById(@PathVariable Long userId) {
		restTemplate.delete(HTTP_LOCALHOST_4040_BOOKS + userId);
	}

}
