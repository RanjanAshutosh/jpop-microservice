package com.jpop.LibraryService.Controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;

import com.jpop.LibraryService.model.LibUser;

import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/lib/users")
public class UserLibraryController {

	private static final String HTTP_LOCALHOST_4041_USERS = "http://USER-SERVICE/users/";

	@Autowired
	WebClient.Builder webClientBuilder;

	@GetMapping
	public List<LibUser> getUsers() {
		LibUser[] user = webClientBuilder.build().get().uri(HTTP_LOCALHOST_4041_USERS).retrieve()
				.bodyToMono(LibUser[].class).block();
		return Arrays.asList(user);
	}

	@GetMapping("/{userId}")
	public LibUser getUserById(@PathVariable Long userId) {
		return webClientBuilder.build().get().uri(HTTP_LOCALHOST_4041_USERS + userId).retrieve()
				.bodyToMono(LibUser.class).block();
	}

	@PostMapping()
	public LibUser addUser(@RequestBody LibUser user) {
		return webClientBuilder.build().post().uri(HTTP_LOCALHOST_4041_USERS).body(Mono.just(user), LibUser.class)
				.retrieve().bodyToMono(LibUser.class).block();

	}

	@PutMapping("/{userId}")
	public LibUser updateUser(@PathVariable Long userId, @RequestBody LibUser user) {
		return webClientBuilder.build().put().uri(HTTP_LOCALHOST_4041_USERS + userId)
				.body(Mono.just(user), LibUser.class).retrieve().bodyToMono(LibUser.class).block();
	}

	@DeleteMapping("/{userId}")
	public LibUser deleteUser(@PathVariable Long userId) {
		return webClientBuilder.build().delete().uri(HTTP_LOCALHOST_4041_USERS + userId).retrieve()
				.bodyToMono(LibUser.class).block();
	}

}
