DROP TABLE IF EXISTS user;
 
CREATE TABLE user (
  id INT  PRIMARY KEY,
  name VARCHAR(100) NOT NULL,
  email VARCHAR(150) NOT NULL ,
  mobile VARCHAR(10) NOT NULL
);
 
INSERT INTO user (id, name, email, mobile) VALUES
  (1, 'Giriraj2', '2giriraj_vyas@epam.com', '2087305215'),
  (2, 'Yuvraj2', '2yuvraj_singh@epam.com', '2888888888');